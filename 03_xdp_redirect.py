#!/usr/bin/env python3

import argparse
import ctypes as c
import pyroute2
import struct
import socket
import json
import re

from bcc import BPF

ip = pyroute2.IPRoute()

next_int = 0
def next():
    global next_int
    next_int += 1
    return c.c_int(next_int)

def ip2int(addr):
    return c.c_uint(struct.unpack("<I", socket.inet_aton(addr))[0])

def main(in_if, config):
    b1 = BPF(src_file='xdp_src/xdp_redirect.c')
    
    fn1 = b1.load_func('xdp_redirect', BPF.XDP)
    
    b1.attach_xdp(in_if, fn1, 0)
    
    cont_map = b1.get_table('container_map')
    ip_map = b1.get_table('ip_map')
    
    for node in config.keys():
        if node == 'docker0':
            continue

        out_id = ip.link_lookup(ifname=config[node]['veth_host'])[0]
    
        idx = next()
        ip_map[ip2int(config[node]['ip'])] = idx
        cont_map[idx] = c.c_int(out_id)
    
    try: 
        b1.trace_print()
    except KeyboardInterrupt:
        print('quitting...')
    
    b1.remove_xdp(in_if, 0)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i','--inif',help='in interface to lay xdp',required=True)
    parser.add_argument('-c','--config',help='config to extract redirect information',required=True)
    args = parser.parse_args()

    with open(args.config, 'r') as file:
        config = json.load(file)
    
    main(args.inif, config)
