//#include "vmlinux.h"
#include <uapi/linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/ip.h>
#include <linux/in.h>

BPF_DEVMAP(container_map, 256);
BPF_HASH(ip_map, __u32, __u8);

int xdp_redirect(struct xdp_md *ctx) {
	
	void *data_end = (void *)(long)ctx->data_end;	    
	void *data = (void *)(long)ctx->data;
	
	struct ethhdr *eth = data;
	struct iphdr *iph;
	__u64 nh_off = ETH_HLEN;
	__u32 ipaddr = 0;
        __u8 *idx = 0;

	if (data + nh_off > data_end) { return XDP_ABORTED; }
        
	if (eth->h_proto == htons(ETH_P_IP)) {
	    iph = data + nh_off;
            if (iph + 1 > data_end) { return 0; }
            ipaddr = iph->daddr;
        }
                	
        if (ipaddr) {
            idx = ip_map.lookup(&ipaddr);
            if (!idx) { goto end; }
            return container_map.redirect_map(*idx, 0);
        }
    end:
        return XDP_PASS;
}
