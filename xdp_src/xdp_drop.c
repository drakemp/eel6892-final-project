#include <linux/bpf.h>

#ifndef __section
# define __section(NAME)                  \
   __attribute__((section(NAME), used))
#endif

static int foo(void) {
    return XDP_DROP;
}

__section("prog")
int xdp_drop(void) {
    return foo();
}

char __license[] __section("license") = "GPL";
