FROM ubuntu:20.04

RUN apt update -y && apt install -y net-tools iproute2 tcpdump hping3

CMD ["tail", "-f", "/dev/null"]
