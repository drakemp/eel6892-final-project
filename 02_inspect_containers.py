#!/usr/bin/env python3

import os
import re
import json
import jsbeautifier
import argparse
from pprint import pprint

DOCKER_IMAGE='node'

FILTER='Hostname={{.Config.Hostname}} ' +\
       'IP={{or .NetworkSettings.IPAddress .NetworkSettings.Networks.testnet.IPAddress}} ' +\
       'Mac={{or .NetworkSettings.MacAddress .NetworkSettings.Networks.testnet.MacAddress}} ' +\
       'Bridge={{if .NetworkSettings.IPAddress}}docker0{{else}}testnet{{end}}'

IP_FILTER = '{{or .NetworkSettings.IPAddress .NetworkSettings.Networks.testnet.IPAddress}}'
#popen wrapper to clean up code
def popen(cmd):
    p = os.popen(cmd)
    ret = re.sub('\n$','', p.read())
    p.close()
    return ret

def main(num):
    cons = {}

    for n in range(1,num+1):
        net = popen(f'docker inspect {DOCKER_IMAGE}{n} --format "{FILTER}"')
        ip  = popen(f'docker inspect {DOCKER_IMAGE}{n} --format "{IP_FILTER}"')
        cons[f'{DOCKER_IMAGE}{n}'] = {'net': net.replace('/',''), 
                                      'ip': ip }

    os.system('sudo mkdir -p /var/run/netns')

    for con in cons.keys():
        pid = popen(f'docker inspect {con} ' + '-f "{{.State.Pid}}"')
        cons[con]['pid'] = int(pid)
        
        #expose namespace
        os.system(f'sudo ln -sf /proc/{cons[con]["pid"]}/ns/net /var/run/netns/{con}')

        veth = popen(f'sudo ip netns exec {con} ip a | grep -A 3 -e "eth0@"')
        cons[con]['veth_cont_vebose'] = veth
        cons[con]['veth_cont'] = re.findall('(eth0@if\d+)',veth)[0]
        
        vethpairid = int(re.match('^(\d+):',veth).groups()[0])
        vethpair = popen(f'ip addr | grep -A 3 -e "@if{vethpairid}"')
        cons[con]['veth_host_verbose'] = vethpair
        cons[con]['veth_host'] = re.findall('(veth[a-f0-9]+)@if', vethpair)[0]
    
    bridge = popen('ip addr | grep -A 5 -e "docker0:"')
    cons['docker0'] = bridge

    opts = jsbeautifier.default_options()
    opts.indent_size = 2
    print(jsbeautifier.beautify(json.dumps(cons), opts))

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-n','--num',type=int,help='Number of containers to inspect', required=True)       
    args = parser.parse_args()

    main(args.num)
