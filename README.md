# EEL 6892 Final Project

setup for fedora 33
```
#cgroups v1
sudo dnf install grubby
sudo grubby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=0"
sudo reboot

#docker
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo sed -i 's/$releasever/31/g' /etc/yum.repos.d/docker-ce.repo
sudo dnf install docker-ce
sudo systemctl enable --now docker
sudo usermod -aG docker $(whoami)

#bpf
sudo dnf install bcc-tools xdp-tools bpftool libxdp

#python libs
pip3 install -r requirements.txt

```

## `00_init_image`
Creates a baseline docker image called 'node' 

## `01_start_containers`
Creates a number of node containers, used to clean up containers as well

## `02_inspect_containers`
Produces JSON for all networking details about each container including veth pair information. Exposes netns of each container to `ip netns` 

## `03_xdp_monitor`
```
ip link set dev ens33 xdp obj ./[OBJ] verb
ip link set dev ens33 xdp off 
```
