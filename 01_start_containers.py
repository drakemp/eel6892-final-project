#!/usr/bin/env python3

import os
import argparse

DOCKER_IMAGE='node'

def main(num, destroy):

    for n in range(1,num+1):
        if destroy:
            os.system(f'docker container kill {DOCKER_IMAGE}{n}')
        else:
            os.system(f'docker container run -d --rm --name {DOCKER_IMAGE}{n} {DOCKER_IMAGE}')

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-n','--num',type=int,help='Number of containers to create', required=True)       
    parser.add_argument('-d','--destroy',help='Number of containers to create', action='store_true')       
    args = parser.parse_args()

    main(args.num, args.destroy)
